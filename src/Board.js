import React from 'react';
import Square from './Square';

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        key={i}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    const rows = []
    for (let i = 0; i < 3; i++) {
      const columns = [];
      for (let j = 0; j < 3; j++) {
        columns.push(this.renderSquare((i*3)+j));
      }
      rows.push(<div key={i} className="board-row">{columns}</div>);
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}

export default Board;
